module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
			dev: {
				options: {
					banner: "/*! uTagCloud <%= grunt.file.readJSON('package.json').version %>\n"
						+ "    Licence: BSD 3-Clause */\n"
						+ "(function(window) {\n"
						+ "\"use strict\";\n\n",
					footer: "})(window);\n"
				},
				files: {
					"dist/uTagCloud.js": [
						"src/utagcloud.js",
						"src/input.js"
					]
				}
			},
			dist: {
				options: {
					banner: "/*! uTagCloud <%= grunt.file.readJSON('package.json').version %>\n"
						+ "    Licence: BSD 3-Clause */\n"
				},
				files: {
					"dist/uTagCloud.min.js": [
						"dist/uTagCloud.min.js"
					]
				}
			}
		},
		uglify: {
			dist: {
				options: {
					mangle: true,
					compress: {
						drop_console: true
					}
				},
				files: {
					"dist/uTagCloud.min.js": [
						"dist/uTagCloud.js"
					]
				}
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-uglify");

	// Custom tasks
	grunt.registerTask("default", ["concat:dev"]);
	grunt.registerTask("dist", ["concat:dev", "uglify:dist", "concat:dist"]);
};
