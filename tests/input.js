describe("Input", function() {
	it("should extract input values from objects (defaults)", function() {
		var input = [
			{ name: "foo", value: 10 },
			{ name: "bar", value: 20 },
			{ name: "baz", value: 30 }
		];

		var output = new Mingos.uTagCloud.Input(input).extractValues();

		expect(output).toEqual([10, 20, 30]);
	});

	it("should extract input values from objects (custom field)", function() {
		var input = [
			{ name: "foo", lumberjack: 10 },
			{ name: "bar", lumberjack: 20 },
			{ name: "baz", lumberjack: 30 }
		];

		var output = new Mingos.uTagCloud.Input(input, {
			valueField: "lumberjack"
		}).extractValues();

		expect(output).toEqual([10, 20, 30]);
	});

	it("should extract input values from arrays", function() {
		var input = [
			[ "foo", 10 ],
			[ "bar", 20 ],
			[ "baz", 30 ]
		];

		var output = new Mingos.uTagCloud.Input(input, {
			valueField: 1
		}).extractValues();

		expect(output).toEqual([10, 20, 30]);
	});

	it("should replace values with processed ones (default)", function() {
		var input = [
			{ name: "foo", value: 10 },
			{ name: "bar", value: 20 },
			{ name: "baz", value: 30 }
		];

		var output = new Mingos.uTagCloud.Input(input).getProcessedInput([
			0, 0.5, 1
		]);

		expect(output).toEqual([
			{ name: "foo", value: 0 },
			{ name: "bar", value: 0.5 },
			{ name: "baz", value: 1 }
		]);
	});

	it("should replace values with processed ones (custom field)", function() {
		var input = [
			{ name: "foo", lumberjack: 10 },
			{ name: "bar", lumberjack: 20 },
			{ name: "baz", lumberjack: 30 }
		];

		var output = new Mingos.uTagCloud.Input(input, {
			valueField: "lumberjack"
		}).getProcessedInput([
			0, 0.5, 1
		]);

		expect(output).toEqual([
			{ name: "foo", lumberjack: 0 },
			{ name: "bar", lumberjack: 0.5 },
			{ name: "baz", lumberjack: 1 }
		]);
	});

	it("should replace values with processed ones (array index)", function() {
		var input = [
			[ "foo", 10 ],
			[ "bar", 20 ],
			[ "baz", 30 ]
		];

		var output = new Mingos.uTagCloud.Input(input, {
			valueField: 1
		}).getProcessedInput([
			0, 0.5, 1
		]);

		expect(output).toEqual([
			[ "foo", 0 ],
			[ "bar", 0.5 ],
			[ "baz", 1 ]
		]);
	});

	it("should enable the use of custom extracting functions", function() {
		var input = [
			{ id: 1, items: ["I", "II"] },
			{ id: 2, items: ["I", "II", "III", "IV"] },
			{ id: 3, items: ["I"] }
		];

		var output = new Mingos.uTagCloud.Input(input, {
			extract: function(input) {
				var data = [], i, j = input.length;
				for(i = 0; i < j; ++i) {
					data.push(input[i].items.length);
				}
				return data;
			}
		}).extractValues();

		expect(output).toEqual([2, 4, 1]);
	});

	it("should enable the use of custom reassembling functions", function() {
		var input = [
			{ id: 1, items: ["I", "II"] },
			{ id: 2, items: ["I", "II", "III", "IV"] },
			{ id: 3, items: ["I"] }
		];

		var output = new Mingos.uTagCloud.Input(input, {
			reassemble: function(input, processedData) {
				var i, j = processedData.length;
				for (i = 0; i < j; ++i) {
					input[i].value = processedData[i];
				}
				return input;
			}
		}).getProcessedInput([
			0.5, 1, 0.25
		]);

		expect(output).toEqual([
			{ id: 1, items: ["I", "II"], value: 0.5 },
			{ id: 2, items: ["I", "II", "III", "IV"], value: 1 },
			{ id: 3, items: ["I"], value: 0.25 }
		]);
	});
});
