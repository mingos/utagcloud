describe("uTagCloud", function() {
	it("should normalise values using linear scale", function() {
		var input    = [0, 1,     2,    4,   8],
			expected = [0, 0.125, 0.25, 0.5 ,1],
			output,
			i, j;

		var cloud = new Mingos.uTagCloud();
		output = cloud.normaliseValues(input);
		for (i = 0, j = input.length; i < j; ++i) {
			expect(output[i]).toBe(expected[i]);
		}
	});

	it("should normalise values using logarithmic scale", function() {
		var input    = [0, 1,     2,   4,     8],
			expected = [0, 0.315, 0.5, 0.732, 1],
			output,
			i, j;

		var cloud = new Mingos.uTagCloud({
			mode: Mingos.uTagCloud.MODE_LOGARITHMIC
		});
		output = cloud.normaliseValues(input);
		for (i = 0, j = input.length; i < j; ++i) {
			expect(output[i]).toBeCloseTo(expected[i], 3);
		}
	});

	it("should refuse to normalise an empty array", function() {
		var input = [],
			cloud = new Mingos.uTagCloud();

		expect(function() {
			cloud.normaliseValues(input);
		}).toThrow("Can't operate on an empty result set.");
	});

	it("should successfully run the whole input conversion", function() {
		var input = [
			{ id: 1, name: "Brave Sir Robin", frequency: 9 },
			{ id: 2, name: "Lumberjack Song", frequency: 12 },
			{ id: 3, name: "I Like Chinese", frequency: 6 }
		];

		// mock the Input
		Mingos.uTagCloud.Input = function()
		{
			return {
				extractValues: function()
				{
					return [9, 12, 6];
				},
				getProcessedInput: function(values)
				{
					return [
						{ id: 1, name: "Brave Sir Robin", frequency: values[0] },
						{ id: 2, name: "Lumberjack Song", frequency: values[1] },
						{ id: 3, name: "I Like Chinese", frequency: values[2] }
					];
				}
			};
		};

		expect(new Mingos.uTagCloud().run(input)).toEqual([
			{ id: 1, name: "Brave Sir Robin", frequency: 0.5 },
			{ id: 2, name: "Lumberjack Song", frequency: 1 },
			{ id: 3, name: "I Like Chinese", frequency: 0 }
		]);
	});
});
