# uTagCloud

uTagCloud is a JavaScript tool used to aid in the creation of tag clouds based on various data structures.

The purpose of the tool is not to create a visual representation of a tag cloud (although it is possible, if an output processing function is passed in). Instead, the goal is to process raw input data in a way that will make the creation of a tag cloud easier.

## Installation

Include the dist file in your project and create a script tag for it in your HTML:

```
<script src="/path/to/utagcloud/uTagCloud.min.js></script>
```

Using Bower to download uTagCloud is not possible at this time.

## Basic usage

### Create an instance of uTagCloud

```
var cloud = new Mingos.uTagCloud();
```

The `uTagCloud` object will allow you to process the input data to extract and normalise the tag frequencies to a scale of <0, 1>. It's up to you how you make use of this data.

The value normalisation function takes in an array or numeric values and can be used manually if needed:

```
cloud.normaliseValues([2, 4, 1, 0, 8]);
// outputs:
// [0.25, 0.5, 0.125, 0 ,1]
```

However, the above will usually be used internally by the `uTagCloud#run` function.

The function uses linear frequency distribution by default. An altenative way is to use logarithmic distribution (read on for details).

### Running the tag cloud processing

The function `uTagCloud#run` is used to perform all the processing of the input data. The data is supposed to be an array of objects.

```
var data = [
    { name: "PHP",        value: 10 },
    { name: "JavaScript", value: 5 },
    { name: "MySQL",      value: 2}
];
var output = cloud.run(data);
```

The function takes in an input data structure: an array of objects, where the frequency of a tag is available under the key "value". The values will be normalised and the resulting structure will be returned.

Non-standard processing can take place if the `uTagCloud` object is configured with the right options.

## Options

The `uTagCloud` constructor can be instantiated with an argument: an object containing config options.

### `mode`

The value distribution mode: either linear or logarithmic.

* type: `Number`
* Possible values: `Mingos.uTagCloud.MODE_LINEAR | Mingos.uTagCloud.MODE_LOGARITHMIC`
* Default: `Mingos.uTagCloud.MODE_LINEAR`

### `output`

Non-standard processing function. Takes the input array with normalised values as argument; returns the processed array.

The function can do any processing needed, for example, changing the normalised frequency values to font sizes (e.g. between 50 and 100%):

```
var options = {
    output: function(data) {
        data.forEach(function(item) {
            item.value = ((item.value + 1) * 50) + "%";
        });
        return data;
    }
};
```

* type: `Function|null`
* Default: `null`

### `input`

Object containing the configuration of the input processor:

#### `input.valueField`

The key under which the frequency value can be found.

* type: `Number|String`
* Default: `"value"`

#### `input.extract`

Funtion that may be used for extracting the frequency values from the input data in case non-standard extracting rules should be applied. The function takes in the input data as argument and returns an array of numeric values.

If not specified, the default behaviour will be applied (take the values of the `input.valueField` and push them onto an array).

* type: `Function|null`
* Default: `null`

#### `input.reassemble`

Non-standard function used to reassemble the extracted and normalised frequency values with the original data structure.

If not specified, the default behaviour is to take the processed frequency array of numbers and copy the values back to each object in the original input, substituting the original `input.valueField` keys.

* type: `Function|null`
* Default: `null`

## Step by step processing description

1. The `uTagCloud` object is instantiated with or without options. The options (or their default values) are stored in an `options` object.
2. `uTagCloud#run` is called with an `input` array of objects.
3. An input processor is created using the `options.input` config options.
4. The input processor receives `input` and extracts the tag frequency values from it, returning an array of numbers.
5. The array of numbers is normalised using the desired `options.mode` into an array of numbers with values between 0 and 1 (inclusive).
6. The data is reassembled using the original `input` and the normalised array, substituting the original frequency values in `input`.
7. The modified `input` is further processed using `options.output`, if such a function is available.
8. The result is returned.

