/*! uTagCloud 1.0.0
    Licence: BSD 3-Clause */
(function(window) {
"use strict";

window.Mingos = window.Mingos || {};

/**
 * Main uTagCloud class.
 * @param  {Object} options Options that will override the defaults
 * @constructor
 */
window.Mingos.uTagCloud = function(options)
{
	var defaults = {
			/**
			 * Mode used to calculate the frequency values (linear or logarithmic)
			 * @type {String}
			 */
			mode: Mingos.uTagCloud.MODE_LINEAR,

			/**
			 * Options for the Input object
			 * @type {Object}
			 */
			input: {}, // the input has its own defaults

			/**
			 * Function used to format the output data.
			 *
			 * @param   {Array} processedInput The processed input data
			 * @returns {*}
			 */
			output: null
		},
		i;

	// apply default options
	options = options || {};
	for (i in defaults) {
		if (defaults.hasOwnProperty(i) && !options.hasOwnProperty(i)) {
			options[i] = defaults[i];
		}
	}

	/**
	 * Process a single value: either return it unchanged, or apply natural logarithm
	 *
	 * @param   {Number} value Input value
	 * @returns {Number}       Output value
	 */
	var processValue = function(value)
	{
		if (options.mode === Mingos.uTagCloud.MODE_LOGARITHMIC) {
			return Math.log(value + 1);
		}

		return value;
	};

	/**
	 * Normalise input values.
	 *
	 * Expects an array of raw frequency values.
	 * Outputs an array of normalised values in the range <0, 1>. The values are normalised
	 * using the normalisation mode (options.mode) defined in the tool options.
	 *
	 * @param  {Array} input Input values
	 * @return {Array}       Normalised output
	 */
	this.normaliseValues = function(input)
	{
		var i, j,
			min = Infinity, max = -Infinity,
			output = input.concat([]);

		// we only process non-empty arrays
		if (!output.length) {
			throw new Error("Can't operate on an empty result set.");
		}

		// calculate min and max values
		for (i = 0, j = output.length; i < j; ++i) {
			min = min > output[i] ? output[i] : min;
			max = max < output[i] ? output[i] : max;
		}

		// subtract min so that the lowest number is always 0
		for (i = 0, j = output.length; i < j; ++i) {
			output[i] -= min;
		}

		// max value needs to be processed in case we need to use a logarithmic scale
		max = processValue(max - min);

		// process the rest of the output array
		for (i = 0, j = output.length; i < j; ++i) {
			output[i] = max ?
				processValue(output[i]) / max :
				0;
		}

		return output;
	};

	/**
	 * Run the entire input to output processing.
	 *
	 * @param  {Array} input Input data array
	 * @return {*}           Processed output
	 */
	this.run = function(input)
	{
		var inputProcessor = new Mingos.uTagCloud.Input(input, options.input),
			extracted = inputProcessor.extractValues(),
			normalised = this.normaliseValues(extracted),
			processedInput = inputProcessor.getProcessedInput(normalised);

		return options.output instanceof Function ?
			options.output(processedInput) :
			processedInput;
	};
};

// constants
window.Mingos.uTagCloud.MODE_LINEAR = "linear";
window.Mingos.uTagCloud.MODE_LOGARITHMIC = "logarithmic";
window.Mingos.uTagCloud.VERSION = "1.0.0";

/**
 * Input processing class.
 *
 * Takes in the tag cloud input and extracts information usable to the uTagCloud class
 * (ie, array of raw values).
 * Places the information received back from uTagCloud and replaced the original input
 * values with the processed ones.
 *
 * This class does no value transformations on its own.
 *
 * @param {Array}  input   Array of items that will form the tag cloud
 * @param {Object} options Additional options, describing how the input should be processsed.
 */
window.Mingos.uTagCloud.Input = function(input, options)
{
	var defaults = {
			/**
			 * Name of the field containing values that should be extracted from each input item
			 * @type {String}
			 */
			valueField: "value",

			/**
			 * Function used to extract the raw numbers for the uTagCloud to process from the input.
			 * If this is not a function, default extraction algorithm will be used.
			 *
			 * @param   {Array} input The input data
			 * @returns {Array}       Array of numbers extracted from the input data
			 */
			extract: null,

			/**
			 * Function used to reassemble the input array using numbers already processed by the uTagCloud object.
			 * If this is not a function, default reassembling algorithm will be used.
			 *
			 * @param   {Array} input         The original input data
			 * @param   {Array} processedData Array of processed numbers to be reinserted into the input
			 * @returns {Array}               Original input with its values replaced with the processed ones
			 */
			reassemble: null
		},
		i;

	// apply default options
	options = options || {};
	for (i in defaults) {
		if (defaults.hasOwnProperty(i) && !options.hasOwnProperty(i)) {
			options[i] = defaults[i];
		}
	}

	/**
	 * Extracts the frequency values from the input data and formats them as an array.
	 *
	 * @return {Array}
	 */
	this.extractValues = function()
	{
		// if the options contain a custom extracting function, use it
		if (options.extract instanceof Function) {
			return options.extract(input);
		} else {
			var data = [],
				i;

			for (i in input) {
				if (input.hasOwnProperty(i)) {
					data.push(parseFloat(input[i][options.valueField]));
				}
			}

			return data;
		}
	};

	/**
	 * Fetch the input with value fields replaced with normalised values.
	 *
	 * @param  {Array} processedData Array of normalised values
	 * @return {Array}
	 */
	this.getProcessedInput = function(processedData)
	{
		if (options.reassemble instanceof Function) {
			return options.reassemble(input, processedData);
		} else {
			var i;

			for (i in input) {
				if (input.hasOwnProperty(i)) {
					input[i][options.valueField] = processedData.shift();
				}
			}

			return input;
		}
	};
};
})(window);
