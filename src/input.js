/**
 * Input processing class.
 *
 * Takes in the tag cloud input and extracts information usable to the uTagCloud class
 * (ie, array of raw values).
 * Places the information received back from uTagCloud and replaced the original input
 * values with the processed ones.
 *
 * This class does no value transformations on its own.
 *
 * @param {Array}  input   Array of items that will form the tag cloud
 * @param {Object} options Additional options, describing how the input should be processsed.
 */
window.Mingos.uTagCloud.Input = function(input, options)
{
	var defaults = {
			/**
			 * Name of the field containing values that should be extracted from each input item
			 * @type {String}
			 */
			valueField: "value",

			/**
			 * Function used to extract the raw numbers for the uTagCloud to process from the input.
			 * If this is not a function, default extraction algorithm will be used.
			 *
			 * @param   {Array} input The input data
			 * @returns {Array}       Array of numbers extracted from the input data
			 */
			extract: null,

			/**
			 * Function used to reassemble the input array using numbers already processed by the uTagCloud object.
			 * If this is not a function, default reassembling algorithm will be used.
			 *
			 * @param   {Array} input         The original input data
			 * @param   {Array} processedData Array of processed numbers to be reinserted into the input
			 * @returns {Array}               Original input with its values replaced with the processed ones
			 */
			reassemble: null
		},
		i;

	// apply default options
	options = options || {};
	for (i in defaults) {
		if (defaults.hasOwnProperty(i) && !options.hasOwnProperty(i)) {
			options[i] = defaults[i];
		}
	}

	/**
	 * Extracts the frequency values from the input data and formats them as an array.
	 *
	 * @return {Array}
	 */
	this.extractValues = function()
	{
		// if the options contain a custom extracting function, use it
		if (options.extract instanceof Function) {
			return options.extract(input);
		} else {
			var data = [],
				i;

			for (i in input) {
				if (input.hasOwnProperty(i)) {
					data.push(parseFloat(input[i][options.valueField]));
				}
			}

			return data;
		}
	};

	/**
	 * Fetch the input with value fields replaced with normalised values.
	 *
	 * @param  {Array} processedData Array of normalised values
	 * @return {Array}
	 */
	this.getProcessedInput = function(processedData)
	{
		if (options.reassemble instanceof Function) {
			return options.reassemble(input, processedData);
		} else {
			var i;

			for (i in input) {
				if (input.hasOwnProperty(i)) {
					input[i][options.valueField] = processedData.shift();
				}
			}

			return input;
		}
	};
};
