window.Mingos = window.Mingos || {};

/**
 * Main uTagCloud class.
 * @param  {Object} options Options that will override the defaults
 * @constructor
 */
window.Mingos.uTagCloud = function(options)
{
	var defaults = {
			/**
			 * Mode used to calculate the frequency values (linear or logarithmic)
			 * @type {String}
			 */
			mode: Mingos.uTagCloud.MODE_LINEAR,

			/**
			 * Options for the Input object
			 * @type {Object}
			 */
			input: {}, // the input has its own defaults

			/**
			 * Function used to format the output data.
			 *
			 * @param   {Array} processedInput The processed input data
			 * @returns {*}
			 */
			output: null
		},
		i;

	// apply default options
	options = options || {};
	for (i in defaults) {
		if (defaults.hasOwnProperty(i) && !options.hasOwnProperty(i)) {
			options[i] = defaults[i];
		}
	}

	/**
	 * Process a single value: either return it unchanged, or apply natural logarithm
	 *
	 * @param   {Number} value Input value
	 * @returns {Number}       Output value
	 */
	var processValue = function(value)
	{
		if (options.mode === Mingos.uTagCloud.MODE_LOGARITHMIC) {
			return Math.log(value + 1);
		}

		return value;
	};

	/**
	 * Normalise input values.
	 *
	 * Expects an array of raw frequency values.
	 * Outputs an array of normalised values in the range <0, 1>. The values are normalised
	 * using the normalisation mode (options.mode) defined in the tool options.
	 *
	 * @param  {Array} input Input values
	 * @return {Array}       Normalised output
	 */
	this.normaliseValues = function(input)
	{
		var i, j,
			min = Infinity, max = -Infinity,
			output = input.concat([]);

		// we only process non-empty arrays
		if (!output.length) {
			throw new Error("Can't operate on an empty result set.");
		}

		// calculate min and max values
		for (i = 0, j = output.length; i < j; ++i) {
			min = min > output[i] ? output[i] : min;
			max = max < output[i] ? output[i] : max;
		}

		// subtract min so that the lowest number is always 0
		for (i = 0, j = output.length; i < j; ++i) {
			output[i] -= min;
		}

		// max value needs to be processed in case we need to use a logarithmic scale
		max = processValue(max - min);

		// process the rest of the output array
		for (i = 0, j = output.length; i < j; ++i) {
			output[i] = max ?
				processValue(output[i]) / max :
				0;
		}

		return output;
	};

	/**
	 * Run the entire input to output processing.
	 *
	 * @param  {Array} input Input data array
	 * @return {*}           Processed output
	 */
	this.run = function(input)
	{
		var inputProcessor = new Mingos.uTagCloud.Input(input, options.input),
			extracted = inputProcessor.extractValues(),
			normalised = this.normaliseValues(extracted),
			processedInput = inputProcessor.getProcessedInput(normalised);

		return options.output instanceof Function ?
			options.output(processedInput) :
			processedInput;
	};
};

// constants
window.Mingos.uTagCloud.MODE_LINEAR = "linear";
window.Mingos.uTagCloud.MODE_LOGARITHMIC = "logarithmic";
window.Mingos.uTagCloud.VERSION = "1.0.0";
